{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{capture name=path}{l s='Contact'}{/capture}
{*<h1 class="page-heading bottom-indent">*}
    {*{l s='Customer service'}*}
    {*- {if isset($customerThread) && $customerThread}{l s='Your reply'}{else}{l s='Contact us'}{/if}*}
{*</h1>*}
{if isset($confirmation)}
    <p class="alert alert-success">{l s='Your message has been successfully sent to our team.'}</p>
    <ul class="footer_links clearfix">
        <li>
            <a class="btn btn-default button button-small"
               href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}">
                <span>
                    <i class="icon-chevron-left"></i>{l s='Home'}
                </span>
            </a>
        </li>
    </ul>
{elseif isset($alreadySent)}
    <p class="alert alert-warning">{l s='Your message has already been sent.'}</p>
    <ul class="footer_links clearfix">
        <li>
            <a class="btn btn-default button button-small"
               href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}">
                <span>
                    <i class="icon-chevron-left"></i>{l s='Home'}
                </span>
            </a>
        </li>
    </ul>
{else}
    {include file="$tpl_dir./errors.tpl"}
   {* <div class="row vwb-block-contact">
        <div class="col-xs-12">
            <div class="text_info col-xs-12 {if $customcontactpage_show}col-sm-4{/if}">
                {if $customcontactpage_show}<h4
                        class="page-subheading">{l s='Store Information' mod='customcontactpage'}</h4>{/if}
                <ul>
                    <li>
                        <strong>{$customcontactpage_company|escape:'html':'UTF-8'}</strong>
                    </li>
                    {if $customcontactpage_address != ''}
                        <li>
                            <i class="icon-map-marker"></i> {$customcontactpage_address|escape:'html':'UTF-8'}
                        </li>
                    {/if}
                    {if $customcontactpage_phone != ''}
                        <li>
                            <i class="icon-phone"></i> {l s='Call us now:' mod='customcontactpage'}
                            <span>{$customcontactpage_phone|escape:'html':'UTF-8'}</span>
                        </li>
                    {/if}
                    {if $customcontactpage_email != ''}
                        <li>
                            <i class="icon-envelope-alt"></i> {l s='Email:' mod='customcontactpage'}
                            <span>{mailto address=$customcontactpage_email|escape:'html':'UTF-8' encode="hex"}</span>
                        </li>
                    {/if}
                    {if $customcontactpage_text != ''}
                        <li class="customcontactpage_text">
                            {$customcontactpage_text}
                        </li>
                    {/if}
                </ul>
            </div>
            <div class="col-xs-12 col-sm-8">
                <div class="contact-us-info-image">
                    <img width="100%" src="https://cdn.houseplans.com/product/q5qkhirat4bcjrr4rpg9fk3q94/w800x533.jpg?v=8" alt="">
                </div>
            </div>
        </div>
    </div>
    {hook h='contactPageHook'}*}




    <div class="contact-us-info-container">
        <div class="contact-us-info-list-container">
            <h1 class="page-heading bottom-indent">
                {l s='Customer service'} - {if isset($customerThread) && $customerThread}{l s='Your reply'}{else}{l s='Contact us'}{/if}
            </h1>
            <div class="official-distributor">{l s='Интернет-магазин SwimSales'}</div>
            <ul class="contact-us-info-list">
                <li><i class="fas fa-user"></i><p>{l s='ИП Дудин Сергей Владимирович'}</p><p>{l s='ИНН: 402708895554 ОГРНИП: 315774600138511'}</p></li>
                <li><i class="fas fa-envelope"></i><span>{l s='Email:'} </span><a href="mailto:mail@swimsales.ru">mail@swimsales.ru</a></li>
                <li><i class="fas fa-phone"></i><span>{l s='Телефоны:'} 8(800)551-40-66 {l s='(бесплатно по РФ)'}; +7(495)127-73-66</li>
                <li>
                    <i class="fas fa-map-marker-alt"></i>
                    <div class="contact-us-info-list-addresses">
                        <p>{l s='Адрес офиса: г. Москва, м. Южная, Варшавское ш. 125с1, 12 подъезд. Не является пунктом выдачи товаров. '}</p>
                        <p>{l s='Адрес регистрации: г. Москва, ул. Старокачаловская, 3к4, 66.'}</p>
                    </div>
                    <div class="contact-us-info-list-delivery">
                        {l s='Кроме курьерской доставки мы предлагаем широкую сеть пунктов выдачи от 100 рублей. Подробнее на странице'} <a href="https://www.swimsales.ru/delivery-calculator">{l s='Доставка'}</a>.
                    </div>
                </li>
                <li><i class="fas fa-clock"></i>{l s='Время работы: 9:00-21:00 ежедневно'}</li>
            </ul>
        </div>
        <div class="contact-us-info-image"></div>
    </div>
    <div id="map">
        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=T4j-CejQt_1sD7JOz0VObUBsFv8ALEvv&amp;width=100%&amp;height=100%&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script>
    </div>
    {*{include file="$tpl_dir./errors.tpl"}*}
    <form action="{$request_uri}" method="post" class="contact-form-box" enctype="multipart/form-data">
        <fieldset>
            <h3 class="page-subheading">{l s='send a message'}</h3>
            <div class="clearfix">
                <div class="col-xs-12 col-md-4">
                    <div class="form-group selector1">
                        <label for="id_contact">{l s='Subject Heading'}</label>
                        {if isset($customerThread.id_contact) && $customerThread.id_contact && $contacts|count}
                        {assign var=flag value=true}
                        {foreach from=$contacts item=contact}
                            {if $contact.id_contact == $customerThread.id_contact}
                                <input type="text" class="form-control" id="contact_name" name="contact_name" value="{$contact.name|escape:'html':'UTF-8'}" readonly="readonly" />
                                <input type="hidden" name="id_contact" value="{$contact.id_contact|intval}" />
                                {$flag=false}
                            {/if}
                        {/foreach}
                        {if $flag && isset($contacts.0.id_contact)}
                            <input type="text" class="form-control" id="contact_name" name="contact_name" value="{$contacts.0.name|escape:'html':'UTF-8'}" readonly="readonly" />
                            <input type="hidden" name="id_contact" value="{$contacts.0.id_contact|intval}" />
                        {/if}
                    </div>
                    {else}
                    <select id="id_contact" class="form-control not_unifrom" name="id_contact">
                        <option value="0">{l s='-- Choose --'}</option>
                        {foreach from=$contacts item=contact}
                            <option value="{$contact.id_contact|intval}"{if isset($smarty.request.id_contact) && $smarty.request.id_contact == $contact.id_contact} selected="selected"{/if}>{$contact.name|escape:'html':'UTF-8'}</option>
                        {/foreach}
                    </select>
                </div>
                {*<p id="desc_contact0" class="desc_contact{if isset($smarty.request.id_contact)} unvisible{/if}">&nbsp;</p>*}
                {*{foreach from=$contacts item=contact}*}
                {*<p id="desc_contact{$contact.id_contact|intval}" class="desc_contact contact-title{if !isset($smarty.request.id_contact) || $smarty.request.id_contact|intval != $contact.id_contact|intval} unvisible{/if}">*}
                {*<i class="fa fa-comment-o"></i>*}
                {*{$contact.description|escape:'html':'UTF-8'}*}
                {*</p>*}
                {*{/foreach}*}
                {/if}
                {*{debug}*}
                <p class="form-group">
                    <label for="email">{l s='Адрес E-mail'}</label>
                    {if isset($customerThread.email)}
                        <input class="form-control grey" type="text" id="email" name="from" value="{$customerThread.email|escape:'html':'UTF-8'}" readonly="readonly" />
                    {else}
                        <input class="form-control grey validate" type="text" id="email" name="from" data-validate="isEmail" value="{$email|escape:'html':'UTF-8'}" />
                    {/if}
                </p>
                {if !$PS_CATALOG_MODE}
                    {if (!isset($customerThread.id_order) || $customerThread.id_order > 0)}
                        <div class="form-group selector1">
                            <label>{l s='Order reference'}</label>
                            {if !isset($customerThread.id_order) && isset($is_logged) && $is_logged}
                                <select name="id_order" class="form-control">
                                    <option value="0">{l s='-- Choose --'}</option>
                                    {foreach from=$orderList item=order}
                                        <option value="{$order.value|intval}"{if $order.selected|intval} selected="selected"{/if}>{$order.label|escape:'html':'UTF-8'}</option>
                                    {/foreach}
                                </select>
                            {elseif !isset($customerThread.id_order) && empty($is_logged)}
                                <input class="form-control grey" type="text" name="id_order" id="id_order" value="{if isset($customerThread.id_order) && $customerThread.id_order|intval > 0}{$customerThread.id_order|intval}{else}{if isset($smarty.post.id_order) && !empty($smarty.post.id_order)}{$smarty.post.id_order|escape:'html':'UTF-8'}{/if}{/if}" />
                            {elseif $customerThread.id_order|intval > 0}
                                <input class="form-control grey" type="text" name="id_order" id="id_order" value="{if isset($customerThread.reference) && $customerThread.reference}{$customerThread.reference|escape:'html':'UTF-8'}{else}{$customerThread.id_order|intval}{/if}" readonly="readonly" />
                            {/if}
                        </div>
                    {/if}
                    {if isset($is_logged) && $is_logged}
                        <div class="form-group selector1">
                            <label class="unvisible">{l s='Product'}</label>
                            {if !isset($customerThread.id_product)}
                                {foreach from=$orderedProductList key=id_order item=products name=products}
                                    <select name="id_product" id="{$id_order}_order_products" class="unvisible product_select form-control"{if !$smarty.foreach.products.first} style="display:none;"{/if}{if !$smarty.foreach.products.first} disabled="disabled"{/if}>
                                        <option value="0">{l s='-- Choose --'}</option>
                                        {foreach from=$products item=product}
                                            <option value="{$product.value|intval}">{$product.label|escape:'html':'UTF-8'}</option>
                                        {/foreach}
                                    </select>
                                {/foreach}
                            {elseif $customerThread.id_product > 0}
                                <input  type="hidden" name="id_product" id="id_product" value="{$customerThread.id_product|intval}" readonly="readonly" />
                            {/if}
                        </div>
                    {/if}
                {/if}
                {if $fileupload == 1}
                    <p class="form-group">
                        <label for="fileUpload">{l s='Attach File'}</label>
                        <input type="hidden" name="MAX_FILE_SIZE" value="{if isset($max_upload_size) && $max_upload_size}{$max_upload_size|intval}{else}2000000{/if}" />
                        <input type="file" name="fileUpload" id="fileUpload" class="form-control" />
                    </p>
                {/if}
                <div class="g-recaptcha" id="g-recapcha"
                     data-sitekey="6LeGHcEUAAAAAI7R9yIdz3wWll5oagZykNqegW_C"></div>
            </div>
            <div class="col-xs-12 col-md-8">
                <div class="form-group">
                    <label for="message">{l s='Message'}</label>
                    <textarea class="form-control" id="message" name="message">{if isset($message)}{$message|escape:'html':'UTF-8'|stripslashes}{/if}</textarea>
                </div>
            </div>
            </div>
            <div class="submit">
                <button type="submit" name="submitMessage" id="submitMessage" class="btn btn-default btn-md">
            	<span>
                	{l s='Send'}
                    <i class="fa fa-chevron-right right"></i>
                </span>
                </button>
            </div>
        </fieldset>
    </form>
{/if}

<script src='https://www.google.com/recaptcha/api.js?hl=ru&onload=callbackCapcha&render=explicit' async defer></script>
<script type="text/javascript" defer>
    if ($("#g-recapcha").length) {
        var callbackCapcha = function () {
            grecaptcha.render('g-recapcha', {
                'sitekey': '6Lc53TwUAAAAAAVh7L_L_3GW1htHhBRrZ7yTrFXd',
                'callback': res,
                'type': 'image'
            });
        };
    }
    var res = function (response) {

    }
</script>
{addJsDefL name='contact_fileDefaultHtml'}{l s='No file selected' js=1}{/addJsDefL}
{addJsDefL name='contact_fileButtonHtml'}{l s='Choose File' js=1}{/addJsDefL}
