$(document).ready(function() {
    var utmMark;

    $('.ajax_block_product').each(function() {
        utmMark = '';
        var $productBlock = $(this);
        var $productNameLink = $productBlock.find('a.product-name');
        var productNameLinkHref = $productNameLink.attr('href');
        var $productImageLink = $productBlock.find('a.product_img_link');
        var productImageLinkHref = $productImageLink.attr('href');

        var product_utm_source = $productBlock.find('input[name="product_utm_source"]').val();

        if (product_utm_source == 0) {
            utmMark = 'utm_content=added_reviews&utm_source=noreview&utm_media=noreview&utm_campaign=noreview';
        } else {
            utmMark = 'utm_content=added_reviews&utm_source=review&utm_media=review&utm_campaign=review';
        }

        $productNameLink.attr('href', productNameLinkHref + ((productNameLinkHref.indexOf('?') !== -1) ?
            '&' : '?') + utmMark);
        $productImageLink.attr('href', productImageLinkHref + ((productImageLinkHref.indexOf('?') !== -1) ?
            '&' : '?') + utmMark);
    });
});