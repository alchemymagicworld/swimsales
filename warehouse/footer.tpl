{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if !$content_only}
					</div><!-- #center_column -->
						{if isset($warehouse_vars.left_on_phones) && $warehouse_vars.left_on_phones == 1}
					{if isset($left_column_size) && !empty($left_column_size)}
					<div id="left_column" class="column col-xs-12 col-sm-{$left_column_size|intval} col-sm-pull-{12 - $left_column_size - $right_column_size}">{$HOOK_LEFT_COLUMN}</div>
					{/if}
					{/if}
					{if isset($right_column_size) && !empty($right_column_size)}
						<div id="right_column" class="col-xs-12 col-sm-{$right_column_size|intval} column">{$HOOK_RIGHT_COLUMN}</div>
					{/if}
					</div><!-- .row -->
				</div><!-- #columns -->
			</div><!-- .columns-container -->
			<!-- Footer -->
			{hook h='footerTopBanner'} 

			<div class="footer-container {if isset($warehouse_vars.f_wrap_width) && $warehouse_vars.f_wrap_width == 0} container {/if}">
				{if isset($warehouse_vars.footer_width) && $warehouse_vars.footer_width == 1}
				{if isset($warehouse_vars.footer1_status) && $warehouse_vars.footer1_status == 1}
				<div class="footer-container-inner1">
				<footer id="footer1"  class="container">
					<div class="row">{hook h='displayAdditionalFooter'}</div>
				</footer>
				</div>
				{/if}
				{if isset($HOOK_FOOTER)}
				<div class="footer-container-inner">
				<footer id="footer"  class="container">
					<div class="row">{$HOOK_FOOTER}</div>
				</footer>
				</div>
				{/if}
				{elseif isset($warehouse_vars.footer_width) && $warehouse_vars.footer_width == 0}
				{if isset($warehouse_vars.footer1_status) && $warehouse_vars.footer1_status == 1}
				<footer id="footer1"  class="container footer-container-inner1">
						
					<div class="row">{hook h='displayAdditionalFooter'}</div>
					
				</footer>
				{/if}
				{if isset($HOOK_FOOTER)}
				<footer id="footer"  class="container footer-container-inner">
						
					<div class="row">{$HOOK_FOOTER}</div>
					
				</footer>
				{/if}
				{/if}
			{if isset($warehouse_vars.second_footer)  && $warehouse_vars.second_footer == 1}
			{if isset($warehouse_vars.header_style) && ($warehouse_vars.header_style != 1)}
			<div class="footer_copyrights">
				<footer class="container clearfix">
					<div class="row">
						{if isset($warehouse_vars.copyright_text)}<div class=" {if isset($warehouse_vars.footer_img_src)}col-sm-6{else}col-sm-12{/if}"> {$warehouse_vars.copyright_text}  </div>{/if}

						{if isset($warehouse_vars.footer_img_src) and $warehouse_vars.footer_img_src}<div class="paymants_logos col-sm-6"><img class="img-responsive" src="{$link->getMediaLink($warehouse_vars.image_path)|escape:'html'}" alt="footerlogo" /></div>{/if}



					</div>
				</footer></div>
				{/if}{/if}

			</div><!-- #footer -->
		</div><!-- #page -->
{/if}
{if !$content_only}<div id="toTop" class="transition-300"></div>
{hook h='belowFooter'}{/if}
{include file="$tpl_dir./global.tpl"}
{if $page_name == 'index'}
<script type="application/ld+json">
{literal}
{
  "@context": "https://schema.org",
  "@type": "WebSite",
  "url": "{/literal}{$base_dir_ssl}{literal}",
  "potentialAction": {
    "@type": "SearchAction",
    "target": "{/literal}{$base_dir_ssl}{literal}index.php?controller=search&search_query={search_term_string}",
    "query-input": "required name=search_term_string"
  }
}
{/literal}
</script>
{/if}<div id="pp-zoom-wrapper">
</div>

		{if isset($js_defer) && !$js_defer && isset($js_files) && isset($js_def)}
			{$js_def}
			{foreach from=$js_files item=js_uri}
				{if $js_uri == "/js/jquery/plugins/fancybox/jquery.fancybox.js" || $js_uri == "/js/jquery/plugins/bxslider/jquery.bxslider.js"}
					<script data="{$js_uri}" type="text/javascript" src="{$js_uri|escape:'html':'UTF-8'}"></script>
				{/if}
			{/foreach}
		{/if}

         <!--<script src="/themes/warehouse/semen/use.fontawesome.com/52aedf78b7.js"></script>-->

<!-- google remarketing script -->
	
	<script type='text/javascript'>
	var google_tag_params = {
	{if $page_name == 'index'}
		dynx_pagetype: 'home'
	{elseif $page_name == 'order-opc'}
		dynx_itemid: [{foreach from=$products item=product name=prodid}'{$product.id_product}'{if $smarty.foreach.prodid.last}{else},{/if}{/foreach}],
		dynx_pagetype: 'cart',
		dynx_totalvalue: '{convertPrice price=$cart->getOrderTotal(true)}'
	{elseif $page_name == 'product'}
		dynx_itemid: '{$product->id}',
		dynx_pagetype: 'product',
		{*dynx_totalvalue: '{$product->getPrice(false, $smarty.const.NULL, $priceDisplayPrecision)}'*}
	{elseif $page_name == 'order-confirmation'}
		dynx_pagetype: 'purchase',
		dynx_totalvalue: '{$total_to_pay}'
	{elseif $page_name == 'category'}
		dynx_pagetype: 'category'
	{/if}
	}
	</script>
{literal}
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','/themes/warehouse/semen/www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54997727-1', 'auto');
  ga('send', 'pageview');

</script>
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 954398616;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<!--<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">-->
<script type="text/javascript" src="/themes/warehouse/semen/www.googleadservices.com/conversion.js">
</script>
<!--<script type="text/javascript" src="https://points.boxberry.de/js/boxberry.min.js"></script>-->
<script type="text/javascript" src="/themes/warehouse/semen/points.boxberry.de/boxberry.min.js"></script>
<style>
.boxberry_overlay{display:none;background:rgba(0,0,0,.3);top:0;right:0;bottom:0;left:0;position:fixed;z-index:1200}.boxberry_container{background:#fff;border:1px solid #aaa;border-radius:4px;box-shadow:0 3px 7px #333;color:#000;display:none;width:70%;position:absolute;text-shadow:0 1px 0 #fff;z-index:1250;top:0;left:0;padding:3px}.boxberry_container_close{text-decoration:none;color:#ed1651;font:12px Tahoma,Verdana,sans-serif;position:absolute;float:right;right:7px;margin:9px 0}.boxberry_content{overflow:hidden;padding:5px 10px;margin-top:5px}.boxberry_logo{margin:3px 5px}@media screen and (max-width:1550px){.boxberry_container{width:100%}}
</style>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/954398616/?guid=ON&amp;script=0"/>
</div>
</noscript>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter25275056 = new Ya.Metrika({
                    id:25275056,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true,
                    ecommerce:"dataLayer"
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        /*s.src = "https://mc.yandex.ru/metrika/watch.js";*/
        s.src = "/themes/warehouse/semen/mc.yandex.ru/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/25275056" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
<!-- Rating Mail.ru counter -->
<script type="text/javascript">
var _tmr = window._tmr || (window._tmr = []);
_tmr.push({id: "3161068", type: "pageView", start: (new Date()).getTime(), pid: "USER_ID"});
(function (d, w, id) {
  if (d.getElementById(id)) return;
  var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
  ts.src = "https://top-fwz1.mail.ru/js/code.js";
  var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
  if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
})(document, window, "topmailru-code");
</script><noscript><div>
<img src="https://top-fwz1.mail.ru/counter?id=3161068;js=na" style="border:0;position:absolute;left:-9999px;" alt="Top.Mail.Ru" />
</div></noscript>
<!-- //Rating Mail.ru counter -->

<!-- Rating@Mail.ru counter dynamic remarketing appendix -->
<script type="text/javascript">
if (typeof mt_pageType !== 'undefined') {
	var _tmr = _tmr || [];
	_tmr.push({
		type: 'itemView',
		productid: mt_productid.split(','),
		pagetype: mt_pageType,
		list: mt_list,
		totalvalue: mt_totalValue
	});
}
</script>
<!-- // Rating@Mail.ru counter dynamic remarketing appendix -->



{/literal}

<!-- BEGIN JIVOSITE CODE {literal} -->
<script type='text/javascript'>
    (function(){ var widget_id = 'ZWkNoqTFjW';var d=document;var w=window;function l(){
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();
        //var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '/themes/warehouse/semen/code.jivosite.com/ZWkNoqTFjW'; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
<!-- {/literal} END JIVOSITE CODE -->
{literal}
<!-- calltouch -->
<script type="text/javascript">
	(function(w,d,n,c){w.CalltouchDataObject=n;w[n]=function(){w[n]["callbacks"].push(arguments)};if(!w[n]["callbacks"]){w[n]["callbacks"]=[]}w[n]["loaded"]=false;if(typeof c!=="object"){c=[c]}w[n]["counters"]=c;for(var i=0;i<c.length;i+=1){p(c[i])}function p(cId){var a=d.getElementsByTagName("script")[0],s=d.createElement("script"),i=function(){a.parentNode.insertBefore(s,a)};s.type="text/javascript";s.async=true;s.src="https://mod.calltouch.ru/init.js?id="+cId;if(w.opera=="[object Opera]"){d.addEventListener("DOMContentLoaded",i,false)}else{i()}}})(window,document,"ct","69mqe790");
</script>
<!-- calltouch -->
{/literal}
	</body>
</html>