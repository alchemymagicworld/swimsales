{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{*<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript">*}
{*</script>*}
{capture name=path}{l s='Калькулятор доставки'}{/capture}
{*{include file="./breadcrumb.tpl"}*}

<h1 class="dotted_bottom">{l s='Расчет стоимости доставки'}</h1>
Стоимость доставки по Москве в пределах МКАД: <strong>220 руб.</strong><br><br>
<strong>Доставка по Москве</strong> осуществляется 6 дней в неделю с 10 до 23:00 (в субботу до 18:00)<br><br>
Доставка по Москве осуществляется на следующий день при заказе <strong>до 16:30.</strong><br><br>
При доставке по Москве закажите несколько товаров <strong>на выбор</strong> из представленных в наличии!
<br></br>
<strong>Доставка в другие города и пункты выдачи</strong>
<br>Доставка указана в <strong>рабочих днях</strong> без учета дня отправки.</br>
Дождитесь загрузки калькулятора под этой строкой:

<br></br>
{if !Configuration::get('VWB_ENABLE_YANDEX_MAP')}
<div id="gWidget"
     data-from-city="Москва"
     data-from-hide="1"
     data-from-single="1"
     data-no-weight="1" style="height:500px;"></div>
<script src="https://grastin.ru/js/gWidget.js" async></script>
<br></br>
<br></br>
<div class="rte">
    <form class="std">
        <fieldset>


            <p class="select" id="info_area"></p>
            <div id='test-1'></div>
            <p id="delivery_area"></p>
        </fieldset>
    </form>
</div>
<h1 class="dotted_bottom">{l s='Отслеживание заказа:'}</h1>
<br></br>
Просьба заполнять номер с 8 без пробелов и симоволов:
<br></br>
<div id="gDeliveryWidget"></div>
<script async defer src="https://grastin.ru/js/gDeliveryStatusWidget.js"></script>
{/if}
{if Module::isInstalled('vwbyandexmapsdelivery') and Module::isEnabled('vwbyandexmapsdelivery') and Configuration::get('VWB_ENABLE_YANDEX_MAP')}
    <div class="container">
        <div class="row" style="background-color: rgba(42,42,42,1); color:#fff">
            <div class="set_box">
                <div class="col-xs-3">
                    <h2 class="text-center">Выберите город:</h2>
                </div>
                <div class="col-xs-9 col-md-5">
                   <div class="select-my" style="padding: 10px">
                       <select style="background-color:#fff; margin-top: 15px" name="cities" id="">
                           {foreach from=$cities key=id item=city}
                               <option value="{$id}">{$city.name}</option>
                           {/foreach}
                       </select>
                   </div>
                </div>
            </div>
        </div>
        <div class="row" style="position:relative;">
            <div class="col-xs-12 col-md-8" style="border: 3px solid #cccccc">
                <div class="full_list_carriers">
                    <div class="map_header">
                        <i class="on_map fa fa-map"></i><span>На карте</span>
                    </div>
                    <div class="list-carriers">
                        {foreach from=$carriers item=city name=carr}
                                <div class="carrier">
                                    <div class="col-xs-10">
                                        <span class="itteration">{$smarty.foreach.carr.index + 1}. </span><b>{$city.name}</b><br>
                                        <b>{$city.graffic}</b>
                                    </div>
                                    <div class="col-xs-2">
                                        <span class="info" data-info="{$city.ext_id}">Подробнее</span><br>
                                        <span class="show_on_map" data-coordinates="{$city.latitude};{$city.longitude}">Hа карте</span>
                                    </div>
                                </div>
                        {/foreach}
                    </div>
                </div>
                <span class="full_list fa fa-list"></span>
                <div class="row">
                    <div id="vwbmap" style="width: 100%;height: 500px"></div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 c_list" style="padding: 0px">
                {*Hidden fields*}
                {foreach from=$carriers item=carrier}
                    <div class="full-info-block" id="{$carrier.ext_id}">
                        <i class="close-info fa fa-times-circle-o" aria-hidden="true"></i>
                        <div class="title-full-block">
                            <span class="upper">Отделение:</span><br>
                            {$carrier.name}
                        </div>
                        <div class="time-work">
                            <span class="upper">График работы:</span><br>
                            {$carrier.graffic}
                        </div>
                        <div class="driverdescription">
                            {if isset($carrier.image) && $carrier.image}
                                <img src="{$carrier.image}" alt="{$carrier.name}">
                            {/if}
                        </div>
                    </div>
                {/foreach}
                {* Visible fields*}
                <ul class="custom_delivery_tabs" id="carriers_tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" id="carrier_list" data-toggle="tab" href="#carrier_list_block" role="tab" aria-controls="carrier_list_block" aria-selected="true"><i class="fa fa-car" aria-hidden="true"></i> Курьеры</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" id="self-drive" data-toggle="tab" href="#self_drive_list" role="tab" aria-controls="self_drive_list" aria-selected="false">Самовывоз</a>
                    </li>
                </ul>
                <div class="tab-content tab_content_list" id="myTabContent">
                    <div class="tab-pane fade" id="carrier_list_block" role="tabpanel" aria-labelledby="carrier_list_block">

                    </div>
                    <div class="tab-pane fade  in active" id="self_drive_list" role="tabpanel" aria-labelledby="self_drive_list"></div>
                </div>
                {*end Tabs*}
            </div>
        </div>

        <div class="row order-search-block">
            <h4 class="find-order-title">
                Отслеживание заказа
            </h4>
            <div class="select-post col-xs-12 col-md-3" style="float: none">
                <select style="width: 100%" name="select-post" id="">
                    <option value="post_rus">Почта россии</option>
                    <option value="grastin">Grastin, Boxberry</option>
                    {*<option value="boxberry">Boxberry</option>*}
                    <option value="cdek_post">Cdek</option>
                </select>
            </div>
            {*Boxberry*}
            {*<div class="block-boxberry col-md-4 col-xs-12">*}
                {*<div class="boxberry-order-block">*}
                    {*<h4>Трекинг</h4>*}
                    {*<div class="input-block">*}
                        {*<p class="boxberry-paragpaph">Номер вашего отправления</p>*}
                        {*<input type="text" name="boxberry-order">*}
                        {*<i class="search-icon fa fa-search fa-2x"></i>*}
                    {*</div>*}
                    {*<div class="submit-button">*}
                        {*<a data-post-type="boxberry" class="send-query-boxberry" href="#">Найти отправление</a>*}
                    {*</div>*}
                {*</div>*}
            {*</div>*}

            {*Grastin*}
            <div class="block-grastin col-xs-12">
                {*<div class="col-xs-12 post-block">*}

                    {*<br></br>*}
                    {*Просьба заполнять номер с 8 без пробелов и симоволов:*}
                    {*<br></br>*}
                    {*<div id="gDeliveryWidget"></div>*}
                    {*<script async defer src="https://grastin.ru/js/gDeliveryStatusWidget.js"></script>*}
                {*</div>*}

                <div style="margin-top: 20px;" class="col-md-9 col-xs-12 grastin-post-block">
                    <div class="col-xs-12 col-md-6">
                        <input name="grastin_order" type="text" class="input-track" placeholder="Введите номер заказа">
                    </div>
                    {*<div class="col-xs-12 col-md-3">*}
                        {*<input type="text" name="grastin_phone" class="input-phone" placeholder="Ведите номер телефона">*}
                    {*</div>*}
                    <div class="col-xs-12 col-md-3 text center">
                        <a data-post-type="grastin" href="#" class="see-res text-center">Показать результат</a>
                    </div>
                </div>
            </div>
            {*opst russia*}
            <div class="block-post_rus col-xs-12 col-md-5">
                <div class="input-post-search">
                    <input type="text" class="post_rus" name="rus_order" placeholder="Введите трек-номер">
                    <a data-post-type="post_rus" href="#" class="serach-post-rus-btn"><i class="fa fa-search"></i></a>
                </div>
            </div>
            <div class="block-cdek_post col-xs-12 col-md-8">
                <div class="col-xs-12 col-md-4"><label for="cdek_post pull-right">Номер накладной</label></div>
                <div class="col-xs-12 col-md-4"><input type="text" class="form-control" name="cdek_post"></div>
                <div class="col-xs-12 col-md-4">
                    <a href="#" data-post-type="cdek_post" class="btn btn-search-cdek pull-left">Отследить</a>
                </div>
            </div>
        </div>
        <div class="search-result col-xs-12 col-md-6">
            <h4 class="text-center">Результат</h4>
            <div class="res-container">

            </div>
        </div>
    </div>
    <script src="https://api-maps.yandex.ru/2.1/?apikey=71773e44-85b5-4f4c-b742-71388588cc12&lang=ru_RU" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            ymaps.load(function(){
                map = new ymaps.Map("vwbmap", {
                    center: [55.76, 37.64],
                    zoom:9
                });
                AjaxMaps.init(map);
            });
        });
    </script>
{/if}



