{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<div class="delivery_infos" id='delivery_courier' data-count='{count($global_items['courier'])}' style='display: none'>
<table>
<tr>
	<th>
		
	</th>
	<th>Тип доставки</th>
	<th>Срок<br>доставки</th>
	<th>Стоимость</th>
		
</tr>
	{foreach from=$global_items['courier'] item=item}
		{if $item->cost>0}
		<tr class='delivery_infos_item'>
		    {*<p>Доступность: {$item->available}</p>
		    <p>Название: {$item->name}</p>
		    <p>Цена: {$item->cost}</p>
		    <p>Адрес: {$item->address}</p>
		    <p>Время работы: {$item->worktimes}</p>*}
		    <td width="60px"><div class="di_item_icon {$item->name}"></div></td>
		    <td class="di_item_name">{$item->name}</td>
		    <td class="di_item_term">{if $item->minperiod!=$item->maxperiod}{$item->minperiod} - {/if}{$item->maxperiod} дн.</td>
		    <td class="di_item_cost">{$item->cost} руб.</td>
		</tr>
		{/if}
	{/foreach}
</table>
</div>
<div class="delivery_infos" id='delivery_postamats' data-count='{count($global_items['postamat'])}' style='display: none'>
{*<table>
<tr>
	<th>
		
	</th>
	<th>Тип доставки</th>
	<th>Срок<br>доставки</th>
	<th>Стоимость</th>
	<th>Адрес пункта выдачи</th>	
</tr>*}
	
		{*{if $item->cost>0}
		<tr class='delivery_infos_item'>
		    {*<p>Доступность: {$item->available}</p>
		    <p>Название: {$item->name}</p>
		    <p>Цена: {$item->cost}</p>
		    <p>Адрес: {$item->address}</p>
		    <p>Время работы: {$item->worktimes}</p>*}
		 {*   <td><div class="di_item_icon {$item->name}"></div></td>
		    <td class="di_item_name">{$item->name}</td>
		    <td class="di_item_term">{if $item->minperiod!=$item->maxperiod}{$item->minperiod} - {/if}{$item->maxperiod} дн.</td>
		    <td class="di_item_cost">{$item->cost} руб.</td>
		    <td>{$item->address}<br><i><small>{$item->remark}</small></i></td>
		</tr>
		{/if}*}
{*</table>*}


      <script type="text/javascript">
   function init () {
    ymaps.geocode('{$cityname}').then(function (res) {
      myMap = new ymaps.Map('map', {
          center: res.geoObjects.get(0).geometry.getCoordinates(),
          controls: [],
          zoom : 10      });

      myMap.controls.add('zoomControl', {
        position: { right: 10, top: 44 }
      });

      ymaps.geoXml.load('http://www.alt-smoke.ru/delivery_map.php?indx={$index}&weight={$weight}&sum={$payment}&payment=0&city={$city}&point=&_=576478109').then(function (res) {
  
      		myMap.geoObjects.add(res.geoObjects);
      		
          //myMap.geoObjects.add(res.geoObjects);
      });

    });
    //console.log('http://swimsales.ru/generatexml.php?data={$global_items['postamat']|@json_encode}');
  }//http://www.alt-smoke.ru/delivery_map.php?indx={$index}&weight={$weight}&sum={$payment}&payment=0&city={$city}&point=&_=576478109
  ymaps.ready(init);

  </script>
  <div style="position: relative;">
  <div id="map-list-top"><a id="map-list-btn" class="btn btn-primary btn-block" href="#">Список постаматов</a></div>
  <div id="map-list" style="display: none;">
  	{foreach from=$global_items['postamat'] item=item}
  		{*print_r($item)*}
        <div class="map-list-item" data-id="{$item->type};{$item->name};{$item->point_id}" data-min-price='{$item->cost} руб.'>
        	<div class="map-icon {$item->name}"></div>
        	<div class="map-list-item-text">{$item->address}</div>
        </div>
       
     {/foreach}
   </div><!--#map-list-->
	    <div style="width: 720px; background: #fff; margin: 10px -20px -20px -20px; border-top: 1px solid #ccc;">
	    	<div id="map" style="width: 720px; height: 600px"></div><!--#map-->
	    </div><!--#mapcontainer-->
    </div><!--position:relative-->
    

</div>
<div class="delivery_infos" id='delivery_post' data-count='{count($global_items['post'])}' style='display: none'>
<table>
<tr>
	<th>
		
	</th>
	<th>Тип доставки</th>
	<th>Срок<br>доставки</th>
	<th>Стоимость</th>
	
</tr>
	{foreach from=$global_items['post'] item=item}
		{if $item->cost>0}
		<tr class='delivery_infos_item'>
		    {*<p>Доступность: {$item->available}</p>
		    <p>Название: {$item->name}</p>
		    <p>Цена: {$item->cost}</p>
		    <p>Адрес: {$item->address}</p>
		    <p>Время работы: {$item->worktimes}</p>*}
		    <td width="60px"><div class="di_item_icon {$item->name}"></div></td>
		    <td class="di_item_name">{$item->name}</td>
		    <td class="di_item_term">{if $item->minperiod!=$item->maxperiod}{$item->minperiod} - {/if}{$item->maxperiod} дн.</td>
		    <td class="di_item_cost">{$item->cost} руб.</td>
		</tr>
		{/if}
	{/foreach}
</table>
</div>

<div class="delivery_infos" id='delivery_selfpickup' data-count='{count($global_items['selfpickup'])}' style='display: none'>
<table>
<tr>
	<th>
		
	</th>
	<th>Тип доставки</th>
	<th>Срок<br>доставки</th>
	<th>Стоимость</th>
	<th>Адрес отделения</th>	
	
</tr>
	{foreach from=$global_items['selfpickup'] item=item}
		{if $item->cost>0}
		<tr class='delivery_infos_item'>
		    {*<p>Доступность: {$item->available}</p>
		    <p>Название: {$item->name}</p>
		    <p>Цена: {$item->cost}</p>
		    <p>Адрес: {$item->address}</p>
		    <p>Время работы: {$item->worktimes}</p>*}
		    <td width="60px"><div class="di_item_icon {$item->name}"></div></td>
		    <td class="di_item_name">{$item->name}</td>
		    <td class="di_item_term">{if $item->minperiod!=$item->maxperiod}{$item->minperiod} - {/if}{$item->maxperiod} дн.</td>
		    <td class="di_item_cost">{$item->cost} руб.</td>
		    <td>{$item->address}</td>
		</tr>
		{/if}
	{/foreach}
</table>
</div>

{*}
<label for="info">{l s='Выбирите место'}</label>
<select id="info">
    <option>--</option>
    {foreach from=$items item=item}
    <option value="{$item.id}">{$item.name}</option>
    {/foreach}
</select>
{*}
   