{*
	* 2007-2014 PrestaShop
	*
	* NOTICE OF LICENSE
	*
	* This source file is subject to the Academic Free License (AFL 3.0)
	* that is bundled with this package in the file LICENSE.txt.
	* It is also available through the world-wide-web at this URL:
	* http://opensource.org/licenses/afl-3.0.php
	* If you did not receive a copy of the license and are unable to
	* obtain it through the world-wide-web, please send an email
	* to license@prestashop.com so we can send you a copy immediately.
	*
	* DISCLAIMER
	*
	* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
	* versions in the future. If you wish to customize PrestaShop for your
	* needs please refer to http://www.prestashop.com for more information.
	*
	*  @author PrestaShop SA <contact@prestashop.com>
	*  @copyright  2007-2014 PrestaShop SA
	*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
	*  International Registered Trademark & Property of PrestaShop SA
	*}
<!DOCTYPE HTML>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8 ie7"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9 ie8"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<!--[if gt IE 8]> <html class="no-js ie9"{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}><![endif]-->
<html{if isset($language_code) && $language_code} lang="{$language_code|escape:'html':'UTF-8'}"{/if}>
	<head>
		<meta charset="utf-8" />
		<title>{$meta_title|escape:'html':'UTF-8'}</title>
		{if isset($meta_description) AND $meta_description}
			<meta name="description" content="{$meta_description|escape:'html':'UTF-8'}" />
		{/if}
		{if isset($meta_keywords) AND $meta_keywords}
			<meta name="keywords" content="{$meta_keywords|escape:'html':'UTF-8'}" />
		{/if}
		<meta name="generator" content="PrestaShop" />
		<meta name="robots" content="{if isset($nobots)}no{/if}index,{if isset($nofollow) && $nofollow}no{/if}follow" />
		<meta name="viewport" content="initial-scale=1,user-scalable=no,maximum-scale=1,width=device-width">
		<meta name="apple-mobile-web-app-capable" content="yes" /> 
		<link rel="icon" type="image/vnd.microsoft.icon" href="{$favicon_url}?{$img_update_time}" />
		<link rel="shortcut icon" type="image/x-icon" href="{$favicon_url}?{$img_update_time}" />
		<link href="/css/font-awesome.min.css" rel="stylesheet" media="screen">
		<script src="https://kit.fontawesome.com/50760bdef6.js" crossorigin="anonymous"></script>
		{if isset($css_files)}
			{foreach from=$css_files key=css_uri item=media}
				{if $css_uri == 'lteIE9'}
					<!--[if lte IE 9]>
					{foreach from=$css_files[$css_uri] key=css_uriie9 item=mediaie9}
					<link rel="stylesheet" href="{$css_uriie9|escape:'html':'UTF-8'}" type="text/css" media="{$mediaie9|escape:'html':'UTF-8'}" />
					{/foreach}
					<![endif]-->
				{else}
					{if $css_uri == '/modules/vwbleavepagemodal/views/css/vwbleavepagemodal.css'}
						<link rel="stylesheet" href="{$css_uri|escape:'html':'UTF-8'}?v=13" type="text/css" media="{$media|escape:'html':'UTF-8'}" />
					{else}
						<link rel="stylesheet" href="{$css_uri|escape:'html':'UTF-8'}?v=13" type="text/css" media="{$media|escape:'html':'UTF-8'}" />
					{/if}
				{/if}
			{/foreach}
		{/if}
		{if isset($js_defer) && !$js_defer && isset($js_files) && isset($js_def)}
			{$js_def}
			{foreach from=$js_files item=js_uri}
				{if $js_uri != "/modules/simpleslideshow/jquery.eislideshow.js" && $js_uri != "/modules/iqitcontentcreator/js/front.js" && $js_uri != "/js/jquery/plugins/fancybox/jquery.fancybox.js" && $js_uri != "/js/jquery/plugins/jquery.easing.js" && $js_uri != "/modules/columnadverts/columnadvertsfront.js" && $js_uri != "/modules/pluginadder/easyzoom.js" && $js_uri != "/themes/warehouse/js/tools/treeManagement.js" && $js_uri != "/js/jquery/plugins/bxslider/jquery.bxslider.js" && $js_uri != "/modules/vwbcrossselling/views/js/crossselling.js" && $js_uri != "/modules/yamodule/views/js/front.js" && $js_uri != "/modules/themeeditor/js/front/script.js" && $js_uri != "/js/jquery/plugins/jquery.scrollTo.js" && $js_uri != "/js/jquery/plugins/jquery.serialScroll.js"}

					{if $js_uri == '/themes/warehouse/js/product.js' || $js_uri == '/themes/warehouse/js/global.js'}
						<script type="text/javascript" src="{$js_uri|escape:'html':'UTF-8'}?v=16"></script>
					{else}
						<script data="{$js_uri}" type="text/javascript" src="{$js_uri|escape:'html':'UTF-8'}?v=22"></script>
					{/if}

					<!--/modules/vwbcrossselling/views/js/crossselling.js-->
					<!--/modules/yamodule/views/js/front.js-->
					<!--/modules/themeeditor/js/front/script.js-->
					<!--/js/jquery/plugins/jquery.serialScroll.js-->
					{*<script data="{$js_uri}" type="text/javascript" src="{$js_uri|escape:'html':'UTF-8'}"></script>*}
				{/if}
				{if $js_uri == '/modules/simpleslideshow/jquery.eislideshow.js'}
					{literal}
					<script type='text/javascript'>
						$(document).ready(function(){$('#ei-slider').slick({dots:true,infinite:true,accessibility:false,speed:300,autoplaySpeed:5000,lazyLoad:'ondemand',fade:true,autoplay:true,cssEase:'ease-in-out'});});
					</script>
					{/literal}
				{/if}
				{if $js_uri == '/modules/iqitcontentcreator/js/front.js'}
					{literal}
					<script type='text/javascript'>
						$(document).ready(function(){$('.iqitcarousel').slick({accessibility:false,autoplaySpeed:4500,rtl:isRtl,lazyLoad:iqit_carousel_load,speed:300,});$("#iqitcontentcreator .nav-tabs").each(function(){$(this).find("li").first().addClass("active");});$("#iqitcontentcreator .tab-content").each(function(){$(this).find(".tab-pane").first().addClass("in").addClass("active");});$('a[data-toggle="tab"]').on('shown.bs.tab',function(e){var target=$(e.target).attr("href")
						$(target).find('.iqitcarousel').slick('setPosition');if(typeof(iqit_lazy_load)!="undefined"&&iqit_lazy_load!==null&&iqit_lazy_load){$(target).find("img.lazy").lazyload({threshold:200,skip_invisible:false});}});});
					</script>
					{/literal}
				{/if}
				{if $js_uri == '/js/jquery/plugins/jquery.scrollTo.js'}
					{literal}
					<script type='text/javascript'>
						;(function(k){'use strict';k(['jquery'],function($){var j=$.scrollTo=function(a,b,c){return $(window).scrollTo(a,b,c)};j.defaults={axis:'xy',duration:0,limit:!0};j.window=function(a){return $(window)._scrollable()};$.fn._scrollable=function(){return this.map(function(){var a=this,isWin=!a.nodeName||$.inArray(a.nodeName.toLowerCase(),['iframe','#document','html','body'])!=-1;if(!isWin)return a;var b=(a.contentWindow||a).document||a.ownerDocument||a;return/webkit/i.test(navigator.userAgent)||b.compatMode=='BackCompat'?b.body:b.documentElement})};$.fn.scrollTo=function(f,g,h){if(typeof g=='object'){h=g;g=0}if(typeof h=='function')h={onAfter:h};if(f=='max')f=9e9;h=$.extend({},j.defaults,h);g=g||h.duration;h.queue=h.queue&&h.axis.length>1;if(h.queue)g/=2;h.offset=both(h.offset);h.over=both(h.over);return this._scrollable().each(function(){if(f==null)return;var d=this,$elem=$(d),targ=f,toff,attr={},win=$elem.is('html,body');switch(typeof targ){case'number':case'string':if(/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(targ)){targ=both(targ);break}targ=win?$(targ):$(targ,this);if(!targ.length)return;case'object':if(targ.is||targ.style)toff=(targ=$(targ)).offset()}var e=$.isFunction(h.offset)&&h.offset(d,targ)||h.offset;$.each(h.axis.split(''),function(i,a){var b=a=='x'?'Left':'Top',pos=b.toLowerCase(),key='scroll'+b,old=d[key],max=j.max(d,a);if(toff){attr[key]=toff[pos]+(win?0:old-$elem.offset()[pos]);if(h.margin){attr[key]-=parseInt(targ.css('margin'+b))||0;attr[key]-=parseInt(targ.css('border'+b+'Width'))||0}attr[key]+=e[pos]||0;if(h.over[pos])attr[key]+=targ[a=='x'?'width':'height']()*h.over[pos]}else{var c=targ[pos];attr[key]=c.slice&&c.slice(-1)=='%'?parseFloat(c)/100*max:c}if(h.limit&&/^\d+$/.test(attr[key]))attr[key]=attr[key]<=0?0:Math.min(attr[key],max);if(!i&&h.queue){if(old!=attr[key])animate(h.onAfterFirst);delete attr[key]}});animate(h.onAfter);function animate(a){$elem.animate(attr,g,h.easing,a&&function(){a.call(this,targ,h)})}}).end()};j.max=function(a,b){var c=b=='x'?'Width':'Height',scroll='scroll'+c;if(!$(a).is('html,body'))return a[scroll]-$(a)[c.toLowerCase()]();var d='client'+c,html=a.ownerDocument.documentElement,body=a.ownerDocument.body;return Math.max(html[scroll],body[scroll])-Math.min(html[d],body[d])};function both(a){return $.isFunction(a)||$.isPlainObject(a)?a:{top:a,left:a}}return j})}(typeof define==='function'&&define.amd?define:function(a,b){if(typeof module!=='undefined'&&module.exports){module.exports=b(require('jquery'))}else{b(jQuery)}}));
					</script>
					{/literal}
				{/if}
				{if $js_uri == '/themes/warehouse/js/tools/treeManagement.js'}
					{literal}
					<script type='text/javascript'>
						$(document).ready(function(){$('ul.tree.dhtml').hide();if(!$('ul.tree.dhtml').hasClass('dynamized')){$('ul.tree.dhtml ul').prev().before("<span class='grower OPEN'> </span>");$('ul.tree.dhtml ul li:last-child, ul.tree.dhtml li:last-child').addClass('last');$('ul.tree.dhtml span.grower.OPEN').addClass('CLOSE').removeClass('OPEN').parent().find('ul:first').hide();$('ul.tree.dhtml').show();$('ul.tree.dhtml .selected').parents().each(function(){if($(this).is('ul'))toggleBranch($(this).prev().prev(),true);});toggleBranch($('ul.tree.dhtml .selected').prev(),true);$('ul.tree.dhtml span.grower').click(function(){toggleBranch($(this));});$('ul.tree.dhtml').addClass('dynamized');$('ul.tree.dhtml').removeClass('dhtml');}});function openBranch(jQueryElement,noAnimation){jQueryElement.addClass('OPEN').removeClass('CLOSE');if(noAnimation)jQueryElement.parent().find('ul:first').show();else
						jQueryElement.parent().find('ul:first').slideDown();}function closeBranch(jQueryElement,noAnimation){jQueryElement.addClass('CLOSE').removeClass('OPEN');if(noAnimation)jQueryElement.parent().find('ul:first').hide();else
						jQueryElement.parent().find('ul:first').slideUp();}function toggleBranch(jQueryElement,noAnimation){if(jQueryElement.hasClass('OPEN'))closeBranch(jQueryElement,noAnimation);else
						openBranch(jQueryElement,noAnimation);}
					</script>
					{/literal}
				{/if}
				{if $js_uri == '/modules/pluginadder/easyzoom.js'}
					{literal}
					<script type='text/javascript'>
						!function(t){"use strict";function e(e,o){this.$target=t(e),this.opts=t.extend({},r,o,this.$target.data()),void 0===this.isOpen&&this._init()}var o,i,s,h,n,a,r={loadingNotice:"Loading image",errorNotice:"The image could not be loaded",errorDuration:2500,preventClicks:!0,beforeShow:t.noop,beforeHide:t.noop,onShow:t.noop,onHide:t.noop,onMove:t.noop};e.prototype._init=function(){this.$link=this.$target.find("a"),this.$image=this.$target.find("img"),this.$flyout=t('<div class="easyzoom-flyout" />'),this.$notice=t('<div class="easyzoom-notice" />'),this.$target.on({"mousemove.easyzoom touchmove.easyzoom":t.proxy(this._onMove,this),"mouseleave.easyzoom touchend.easyzoom":t.proxy(this._onLeave,this),"mouseenter.easyzoom touchstart.easyzoom":t.proxy(this._onEnter,this)}),this.opts.preventClicks&&this.$target.on("click.easyzoom",function(t){t.preventDefault()})},e.prototype.show=function(t,e){var n,a,r,d,c=this;if(this.opts.beforeShow.call(this)!==!1){if(!this.isReady)return this._loadImage(this.$link.attr("href"),function(){(c.isMouseOver||!e)&&c.show(t)});this.$target.append(this.$flyout),n=this.$target.width(),a=this.$target.height(),r=this.$flyout.width(),d=this.$flyout.height(),o=this.$zoom.width()-r,i=this.$zoom.height()-d,s=o/n,h=i/a,this.isOpen=!0,this.opts.onShow.call(this),t&&this._move(t)}},e.prototype._onEnter=function(t){var e=t.originalEvent.touches;this.isMouseOver=!0,e&&1!=e.length||(t.preventDefault(),this.show(t,!0))},e.prototype._onMove=function(t){this.isOpen&&(t.preventDefault(),this._move(t))},e.prototype._onLeave=function(){this.isMouseOver=!1,this.isOpen&&this.hide()},e.prototype._onLoad=function(t){t.currentTarget.width&&(this.isReady=!0,this.$notice.detach(),this.$flyout.html(this.$zoom),this.$target.removeClass("is-loading").addClass("is-ready"),t.data.call&&t.data())},e.prototype._onError=function(){var t=this;this.$notice.text(this.opts.errorNotice),this.$target.removeClass("is-loading").addClass("is-error"),this.detachNotice=setTimeout(function(){t.$notice.detach(),t.detachNotice=null},this.opts.errorDuration)},e.prototype._loadImage=function(e,o){var i=new Image;this.$target.addClass("is-loading").append(this.$notice.text(this.opts.loadingNotice)),this.$zoom=t(i).on("error",t.proxy(this._onError,this)).on("load",o,t.proxy(this._onLoad,this)),i.style.position="absolute",i.src=e},e.prototype._move=function(t){if(0===t.type.indexOf("touch")){var e=t.touches||t.originalEvent.touches;n=e[0].pageX,a=e[0].pageY}else n=t.pageX||n,a=t.pageY||a;var o=this.$target.offset(),i=a-o.top,r=n-o.left,d=Math.ceil(i*h),c=Math.ceil(r*s),l=-1*d,p=-1*c;this.$zoom.css({top:l,left:p}),this.opts.onMove.call(this,l,p)},e.prototype.hide=function(){this.isOpen&&this.opts.beforeHide.call(this)!==!1&&(this.$flyout.detach(),this.isOpen=!1,this.opts.onHide.call(this))},e.prototype.swap=function(e,o,i){this.hide(),this.isReady=!1,this.detachNotice&&clearTimeout(this.detachNotice),this.$notice.parent().length&&this.$notice.detach(),this.$target.removeClass("is-loading is-ready is-error"),this.$image.attr({src:e,srcset:t.isArray(i)?i.join():i}),this.$link.attr("href",o)},e.prototype.teardown=function(){this.hide(),this.$target.off(".easyzoom").removeClass("is-loading is-ready is-error"),this.detachNotice&&clearTimeout(this.detachNotice),delete this.$link,delete this.$zoom,delete this.$image,delete this.$notice,delete this.$flyout,delete this.isOpen,delete this.isReady},t.fn.easyZoom=function(o){return this.each(function(){var i=t.data(this,"easyZoom");i?void 0===i.isOpen&&i._init():t.data(this,"easyZoom",new e(this,o))})},"function"==typeof define&&define.amd?define(function(){return e}):"undefined"!=typeof module&&module.exports&&(module.exports=e)}(jQuery);
					</script>
					{/literal}
				{/if}
				{if $js_uri == '/modules/columnadverts/columnadvertsfront.js'}
					{literal}
					<script type='text/javascript'>
						$(document).ready(function(){$('#columnadverts_slider').slick({dots:true,infinite:true,accessibility:false,rtl:isRtl,speed:400,fade:true,autoplaySpeed:5000,autoplay:true,cssEase:'ease-in-out'});});
					</script>
					{/literal}
				{/if}
				{if $js_uri == '/js/jquery/plugins/jquery.easing.js'}
					{literal}
					<script type='text/javascript'>
						jQuery.easing['jswing']=jQuery.easing['swing'];jQuery.extend(jQuery.easing,{def:'easeOutQuad',swing:function(x,t,b,c,d){return jQuery.easing[jQuery.easing.def](x,t,b,c,d);},easeInQuad:function(x,t,b,c,d){return c*(t/=d)*t+b;},easeOutQuad:function(x,t,b,c,d){return-c*(t/=d)*(t-2)+b;},easeInOutQuad:function(x,t,b,c,d){if((t/=d/2)<1)return c/2*t*t+b;return-c/2*((--t)*(t-2)-1)+b;},easeInCubic:function(x,t,b,c,d){return c*(t/=d)*t*t+b;},easeOutCubic:function(x,t,b,c,d){return c*((t=t/d-1)*t*t+1)+b;},easeInOutCubic:function(x,t,b,c,d){if((t/=d/2)<1)return c/2*t*t*t+b;return c/2*((t-=2)*t*t+2)+b;},easeInQuart:function(x,t,b,c,d){return c*(t/=d)*t*t*t+b;},easeOutQuart:function(x,t,b,c,d){return-c*((t=t/d-1)*t*t*t-1)+b;},easeInOutQuart:function(x,t,b,c,d){if((t/=d/2)<1)return c/2*t*t*t*t+b;return-c/2*((t-=2)*t*t*t-2)+b;},easeInQuint:function(x,t,b,c,d){return c*(t/=d)*t*t*t*t+b;},easeOutQuint:function(x,t,b,c,d){return c*((t=t/d-1)*t*t*t*t+1)+b;},easeInOutQuint:function(x,t,b,c,d){if((t/=d/2)<1)return c/2*t*t*t*t*t+b;return c/2*((t-=2)*t*t*t*t+2)+b;},easeInSine:function(x,t,b,c,d){return-c*Math.cos(t/d*(Math.PI/2))+c+b;},easeOutSine:function(x,t,b,c,d){return c*Math.sin(t/d*(Math.PI/2))+b;},easeInOutSine:function(x,t,b,c,d){return-c/2*(Math.cos(Math.PI*t/d)-1)+b;},easeInExpo:function(x,t,b,c,d){return(t==0)?b:c*Math.pow(2,10*(t/d-1))+b;},easeOutExpo:function(x,t,b,c,d){return(t==d)?b+c:c*(-Math.pow(2,-10*t/d)+1)+b;},easeInOutExpo:function(x,t,b,c,d){if(t==0)return b;if(t==d)return b+c;if((t/=d/2)<1)return c/2*Math.pow(2,10*(t-1))+b;return c/2*(-Math.pow(2,-10*--t)+2)+b;},easeInCirc:function(x,t,b,c,d){return-c*(Math.sqrt(1-(t/=d)*t)-1)+b;},easeOutCirc:function(x,t,b,c,d){return c*Math.sqrt(1-(t=t/d-1)*t)+b;},easeInOutCirc:function(x,t,b,c,d){if((t/=d/2)<1)return-c/2*(Math.sqrt(1-t*t)-1)+b;return c/2*(Math.sqrt(1-(t-=2)*t)+1)+b;},easeInElastic:function(x,t,b,c,d){var s=1.70158;var p=0;var a=c;if(t==0)return b;if((t/=d)==1)return b+c;if(!p)p=d*.3;if(a<Math.abs(c)){a=c;var s=p/4;}else var s=p/(2*Math.PI)*Math.asin(c/a);return-(a*Math.pow(2,10*(t-=1))*Math.sin((t*d-s)*(2*Math.PI)/p))+b;},easeOutElastic:function(x,t,b,c,d){var s=1.70158;var p=0;var a=c;if(t==0)return b;if((t/=d)==1)return b+c;if(!p)p=d*.3;if(a<Math.abs(c)){a=c;var s=p/4;}else var s=p/(2*Math.PI)*Math.asin(c/a);return a*Math.pow(2,-10*t)*Math.sin((t*d-s)*(2*Math.PI)/p)+c+b;},easeInOutElastic:function(x,t,b,c,d){var s=1.70158;var p=0;var a=c;if(t==0)return b;if((t/=d/2)==2)return b+c;if(!p)p=d*(.3*1.5);if(a<Math.abs(c)){a=c;var s=p/4;}else var s=p/(2*Math.PI)*Math.asin(c/a);if(t<1)return-.5*(a*Math.pow(2,10*(t-=1))*Math.sin((t*d-s)*(2*Math.PI)/p))+b;return a*Math.pow(2,-10*(t-=1))*Math.sin((t*d-s)*(2*Math.PI)/p)*.5+c+b;},easeInBack:function(x,t,b,c,d,s){if(s==undefined)s=1.70158;return c*(t/=d)*t*((s+1)*t-s)+b;},easeOutBack:function(x,t,b,c,d,s){if(s==undefined)s=1.70158;return c*((t=t/d-1)*t*((s+1)*t+s)+1)+b;},easeInOutBack:function(x,t,b,c,d,s){if(s==undefined)s=1.70158;if((t/=d/2)<1)return c/2*(t*t*(((s*=(1.525))+1)*t-s))+b;return c/2*((t-=2)*t*(((s*=(1.525))+1)*t+s)+2)+b;},easeInBounce:function(x,t,b,c,d){return c-jQuery.easing.easeOutBounce(x,d-t,0,c,d)+b;},easeOutBounce:function(x,t,b,c,d){if((t/=d)<(1/2.75)){return c*(7.5625*t*t)+b;}else if(t<(2/2.75)){return c*(7.5625*(t-=(1.5/2.75))*t+.75)+b;}else if(t<(2.5/2.75)){return c*(7.5625*(t-=(2.25/2.75))*t+.9375)+b;}else{return c*(7.5625*(t-=(2.625/2.75))*t+.984375)+b;}},easeInOutBounce:function(x,t,b,c,d){if(t<d/2)return jQuery.easing.easeInBounce(x,t*2,0,c,d)*.5+b;return jQuery.easing.easeOutBounce(x,t*2-d,0,c,d)*.5+c*.5+b;}});
					</script>
					{/literal}
				{/if}
			{/foreach}
		{/if}
		
		{$HOOK_HEADER}

		{if isset($warehouse_vars.font_include)}
			{foreach from=$warehouse_vars.font_include item=font name=fonts}
				<!--<link rel="stylesheet" href="http{if Tools::usingSecureMode()}s{/if}://{$font}" type="text/css" media="all" />-->
				 {/foreach}
		{/if}
		<style>
			/* cyrillic-ext */
			@font-face {
			  font-family: 'PT Sans';
			  font-style: italic;
			  font-weight: 400;
			  src: local('PT Sans Italic'), local('PTSans-Italic'), url(https://fonts.gstatic.com/s/ptsans/v9/jizYRExUiTo99u79D0e0ysmIEDQ.woff2) format('woff2');
			  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
			}
			/* cyrillic */
			@font-face {
			  font-family: 'PT Sans';
			  font-style: italic;
			  font-weight: 400;
			  src: local('PT Sans Italic'), local('PTSans-Italic'), url(https://fonts.gstatic.com/s/ptsans/v9/jizYRExUiTo99u79D0e0w8mIEDQ.woff2) format('woff2');
			  unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
			}
			/* latin-ext */
			@font-face {
			  font-family: 'PT Sans';
			  font-style: italic;
			  font-weight: 400;
			  src: local('PT Sans Italic'), local('PTSans-Italic'), url(https://fonts.gstatic.com/s/ptsans/v9/jizYRExUiTo99u79D0e0ycmIEDQ.woff2) format('woff2');
			  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
			}
			/* latin */
			@font-face {
			  font-family: 'PT Sans';
			  font-style: italic;
			  font-weight: 400;
			  src: local('PT Sans Italic'), local('PTSans-Italic'), url(https://fonts.gstatic.com/s/ptsans/v9/jizYRExUiTo99u79D0e0x8mI.woff2) format('woff2');
			  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
			}
			/* cyrillic-ext */
			@font-face {
			  font-family: 'PT Sans';
			  font-style: italic;
			  font-weight: 700;
			  src: local('PT Sans Bold Italic'), local('PTSans-BoldItalic'), url(https://fonts.gstatic.com/s/ptsans/v9/jizdRExUiTo99u79D0e8fOydIhUdwzM.woff2) format('woff2');
			  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
			}
			/* cyrillic */
			@font-face {
			  font-family: 'PT Sans';
			  font-style: italic;
			  font-weight: 700;
			  src: local('PT Sans Bold Italic'), local('PTSans-BoldItalic'), url(https://fonts.gstatic.com/s/ptsans/v9/jizdRExUiTo99u79D0e8fOydKxUdwzM.woff2) format('woff2');
			  unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
			}
			/* latin-ext */
			@font-face {
			  font-family: 'PT Sans';
			  font-style: italic;
			  font-weight: 700;
			  src: local('PT Sans Bold Italic'), local('PTSans-BoldItalic'), url(https://fonts.gstatic.com/s/ptsans/v9/jizdRExUiTo99u79D0e8fOydIRUdwzM.woff2) format('woff2');
			  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
			}
			/* latin */
			@font-face {
			  font-family: 'PT Sans';
			  font-style: italic;
			  font-weight: 700;
			  src: local('PT Sans Bold Italic'), local('PTSans-BoldItalic'), url(https://fonts.gstatic.com/s/ptsans/v9/jizdRExUiTo99u79D0e8fOydLxUd.woff2) format('woff2');
			  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
			}
			/* cyrillic-ext */
			@font-face {
			  font-family: 'PT Sans';
			  font-style: normal;
			  font-weight: 400;
			  src: local('PT Sans'), local('PTSans-Regular'), url(https://fonts.gstatic.com/s/ptsans/v9/jizaRExUiTo99u79D0-ExdGM.woff2) format('woff2');
			  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
			}
			/* cyrillic */
			@font-face {
			  font-family: 'PT Sans';
			  font-style: normal;
			  font-weight: 400;
			  src: local('PT Sans'), local('PTSans-Regular'), url(https://fonts.gstatic.com/s/ptsans/v9/jizaRExUiTo99u79D0aExdGM.woff2) format('woff2');
			  unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
			}
			/* latin-ext */
			@font-face {
			  font-family: 'PT Sans';
			  font-style: normal;
			  font-weight: 400;
			  src: local('PT Sans'), local('PTSans-Regular'), url(https://fonts.gstatic.com/s/ptsans/v9/jizaRExUiTo99u79D0yExdGM.woff2) format('woff2');
			  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
			}
			/* latin */
			@font-face {
			  font-family: 'PT Sans';
			  font-style: normal;
			  font-weight: 400;
			  src: local('PT Sans'), local('PTSans-Regular'), url(https://fonts.gstatic.com/s/ptsans/v9/jizaRExUiTo99u79D0KExQ.woff2) format('woff2');
			  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
			}
			/* cyrillic-ext */
			@font-face {
			  font-family: 'PT Sans';
			  font-style: normal;
			  font-weight: 700;
			  src: local('PT Sans Bold'), local('PTSans-Bold'), url(https://fonts.gstatic.com/s/ptsans/v9/jizfRExUiTo99u79B_mh0OOtLQ0Z.woff2) format('woff2');
			  unicode-range: U+0460-052F, U+1C80-1C88, U+20B4, U+2DE0-2DFF, U+A640-A69F, U+FE2E-FE2F;
			}
			/* cyrillic */
			@font-face {
			  font-family: 'PT Sans';
			  font-style: normal;
			  font-weight: 700;
			  src: local('PT Sans Bold'), local('PTSans-Bold'), url(https://fonts.gstatic.com/s/ptsans/v9/jizfRExUiTo99u79B_mh0OqtLQ0Z.woff2) format('woff2');
			  unicode-range: U+0400-045F, U+0490-0491, U+04B0-04B1, U+2116;
			}
			/* latin-ext */
			@font-face {
			  font-family: 'PT Sans';
			  font-style: normal;
			  font-weight: 700;
			  src: local('PT Sans Bold'), local('PTSans-Bold'), url(https://fonts.gstatic.com/s/ptsans/v9/jizfRExUiTo99u79B_mh0OCtLQ0Z.woff2) format('woff2');
			  unicode-range: U+0100-024F, U+0259, U+1E00-1EFF, U+2020, U+20A0-20AB, U+20AD-20CF, U+2113, U+2C60-2C7F, U+A720-A7FF;
			}
			/* latin */
			@font-face {
			  font-family: 'PT Sans';
			  font-style: normal;
			  font-weight: 700;
			  src: local('PT Sans Bold'), local('PTSans-Bold'), url(https://fonts.gstatic.com/s/ptsans/v9/jizfRExUiTo99u79B_mh0O6tLQ.woff2) format('woff2');
			  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
			}
		</style>

		<!--<script src="https://use.fontawesome.com/52aedf78b7.js"></script>-->
		
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		<!--[if lte IE 9]>
		
		<script src="{$js_dir}flexibility.js"></script>

		<![endif]-->
		<meta property="og:title" content="{$meta_title|escape:'htmlall':'UTF-8'}"/>
		<meta property="og:url" content="{$smarty.server.HTTP_HOST}{$smarty.server.REQUEST_URI}"/>
		<meta property="og:site_name" content="{$shop_name|escape:'htmlall':'UTF-8'}"/>
		
		<meta property="og:description" content="{$meta_description|escape:html:'UTF-8'}">
		{if $page_name=='product'}
		<meta property="og:type" content="product">
		{if isset($have_image)}
		{if $have_image.0}<meta property="og:image" content="{$link->getImageLink($product->link_rewrite, $cover.id_image, 'large_default')}">{/if}
		{else}
		<meta property="og:image" content="{$logo_url}" />
		{/if}
		{else}
		<meta property="og:type" content="website">
		<meta property="og:image" content="{$logo_url}" />
		{/if}

{literal}
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '594604304610407');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=594604304610407&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
{/literal}
{literal}
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KM5ZJR5');</script>
<!-- End Google Tag Manager -->
{/literal}
	</head>
	<body{if isset($page_name)} id="{$page_name|escape:'html':'UTF-8'}"{/if} {if ($page_name == 'module-smartblog-category') or ($page_name == 'module-smartblog-details') or ($page_name == 'calculate')} {else}class="{if !isset($page_name) || (isset($page_name) && $page_name != 'index')}not-index {/if}{if isset($page_name)}{$page_name|escape:'html':'UTF-8'}{/if}{if isset($body_classes) && $body_classes|@count} {implode value=$body_classes separator=' '}{/if}{if $hide_left_column} hide-left-column{/if}{if $hide_right_column} hide-right-column{/if}{if !$hide_left_column} show-left-column{/if}{if !$hide_right_column} show-right-column{/if} {if isset($content_only) && $content_only} content_only{/if} lang_{$lang_iso}  {if isset($warehouse_vars.is_rtl) && $warehouse_vars.is_rtl} is_rtl{/if} 	{if $is_logged} isLogged{/if} {if isset($warehouse_vars.header_style) && $warehouse_vars.header_style == 1} is-sidebar-header{/if}"{/if}>
	{if !isset($content_only) || !$content_only}
	{if isset($warehouse_vars.preloader) && $warehouse_vars.preloader}
	<div id="preloader">
	<div id="status">&nbsp;</div>
	</div>
	{/if}
	{hook h='freeFblock'}
	{if isset($restricted_country_mode) && $restricted_country_mode}
	<div id="restricted-country">
		<p>{l s='You cannot place a new order from your country.'}{if isset($geolocation_country) && $geolocation_country} <span class="bold">{$geolocation_country|escape:'html':'UTF-8'}</span>{/if}</p>
	</div>
	{/if}
	<div id="page">
		<div class="header-container{if isset($warehouse_vars.header_style)}{if $warehouse_vars.header_style == 1} sidebar-header{elseif ($warehouse_vars.header_style == 2 || $warehouse_vars.header_style == 3)} inline-header{/if}{/if}{if isset($warehouse_vars.cart_style) && $warehouse_vars.cart_style == 1} alt-cart{/if}">
			<header id="header">
		
			
						<div class="banner">
					<div class="container">
						<div class="row">
							{hook h="displayBanner"}
						</div>
					</div>
				</div>
					{if isset($warehouse_vars.top_width) && $warehouse_vars.top_width == 1  && $warehouse_vars.top_bar}
				<div class="nav">
					<div class="container">
						<div class="row">
							<nav>
								<span class="header-work-time-label">{l s='Режим работы — 9:00 - 21:00 мск.'}</span>
                                {*{include file="./product-compare.tpl"}*}
								{hook h="displayNav"}
								{if isset($warehouse_vars.wishlist_status) && $warehouse_vars.wishlist_status}<a href="{$link->getModuleLink('blockwishlist', 'mywishlist', array(), true)}" title="{l s='My wishlist'}" class="wishlist_top_link pull-right"><i class="icon-heart-empty"></i>  {l s='My wishlist'}</a>{/if}
							</nav>
						</div>
					</div>
				</div>
				{/if}
				<div class="mobile-header-logo-container">
					<div class="container">
						<div class="row">
							<div class="mobile-header-logo">
								<a href="{if $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}" title="{$shop_name|escape:'html':'UTF-8'}">
                                    {*<img class="logo img-responsive replace-2xlogo" src="{$logo_url}" {if isset($warehouse_vars.retina_logo) and $warehouse_vars.retina_logo}data-retinalogo="{$warehouse_vars.retina_logo}" {/if} {if isset($logo_image_width) && $logo_image_width} width="{$logo_image_width}"{/if}{if isset($logo_image_height) && $logo_image_height} height="{$logo_image_height}"{/if} alt="{$shop_name|escape:'html':'UTF-8'}" />*}
									<img class="logo img-responsive replace-2xlogo" src="{$logo_url}"{if isset($logo_image_width) && $logo_image_width} width="{$logo_image_width}"{/if}{if isset($logo_image_height) && $logo_image_height} height="{$logo_image_height}"{/if} alt="{$shop_name|escape:'html':'UTF-8'}" />
								</a>
							</div>
						</div>
					</div>
				</div>
				<div>
					<div id="mh-sticky" class="not-sticked">
						<div class="container container-header mobile-main-wrapper">
                            {if isset($warehouse_vars.top_width) && $warehouse_vars.top_width == 0 && $warehouse_vars.top_bar}
								<div class="nav">
									<div class="row">
										<nav>
                                            {hook h="displayNav"}
                                            {include file="./product-compare.tpl"}
                                            {if isset($warehouse_vars.wishlist_status) && $warehouse_vars.wishlist_status}<a href="{$link->getModuleLink('blockwishlist', 'mywishlist', array(), true)}" title="{l s='My wishlist'}" class="wishlist_top_link pull-right"><i class="icon-heart-empty"></i>  {l s='My wishlist'}</a>{/if}
										</nav>
									</div>

								</div>
                            {/if}
							<div id="desktop-header" class="desktop-header">
                                {if isset($warehouse_vars.header_style) && ($warehouse_vars.header_style == 2 || $warehouse_vars.header_style == 3)}
									<div class="row {if isset($warehouse_vars.header_style) && $warehouse_vars.header_style == 3} header-aligned-right {elseif $warehouse_vars.header_style == 2}header-aligned-left{/if}">
										<div class="inline-table">
											<div class="inline-row">
                                                {if isset($warehouse_vars.header_style) && $warehouse_vars.header_style == 2}
													<div class="inline-cell display-menu">
														<div class="inline-cell-table">
															<div id="header_logo">
																<a href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}" title="{$shop_name|escape:'html':'UTF-8'}">
																	<img class="logo img-responsive replace-2xlogo" src="{$logo_url}" {if isset($warehouse_vars.retina_logo) and $warehouse_vars.retina_logo}data-retinalogo="{$warehouse_vars.retina_logo}" {/if} {if isset($logo_image_width) && $logo_image_width} width="{$logo_image_width}"{/if}{if isset($logo_image_height) && $logo_image_height} height="{$logo_image_height}"{/if} alt="{$shop_name|escape:'html':'UTF-8'}" />
																</a>
															</div>
                                                            {hook h='iqitMegaMenu'}
														</div></div>
													<div class="displayTop inline-cell">
                                                        {if isset($HOOK_TOP)}{$HOOK_TOP}{/if}
													</div>

                                                {else}
													<div class="inline-cell inline-cell-logo">
														<div id="header_logo">
															<a href="{if isset($force_ssl) && $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}" title="{$shop_name|escape:'html':'UTF-8'}">
																<img class="logo img-responsive replace-2xlogo" src="{$logo_url}" {if isset($warehouse_vars.retina_logo) and $warehouse_vars.retina_logo}data-retinalogo="{$warehouse_vars.retina_logo}" {/if} {if isset($logo_image_width) && $logo_image_width} width="{$logo_image_width}"{/if}{if isset($logo_image_height) && $logo_image_height} height="{$logo_image_height}"{/if} alt="{$shop_name|escape:'html':'UTF-8'}" />
															</a>
														</div>

													</div>
													<div class="displayTop inline-cell display-menu">
														<div class="inline-cell-table">
                                                            {hook h='iqitMegaMenu'}
															<div class="inline-cell-noflex">{if isset($HOOK_TOP)}{$HOOK_TOP}{/if}</div>
														</div>
													</div>

                                                {/if}

											</div>
										</div>
									</div>
                                {else}
									<div class="row">
										<div id="mh-menu" class="header-mobile-menu-button"></div>
                                        {*<div id="header_logo" class="col-xs-12 col-sm-{4+$warehouse_vars.logo_width} {if isset($warehouse_vars.logo_position) && !$warehouse_vars.logo_position} col-sm-push-{4-$warehouse_vars.logo_width/2} centered-logo  {/if}">*}
										<div id="header_logo">
											<a href="{if $force_ssl}{$base_dir_ssl}{else}{$base_dir}{/if}" title="{$shop_name|escape:'html':'UTF-8'}">
												<img class="logo img-responsive replace-2xlogo" src="{$logo_url}" {if isset($warehouse_vars.retina_logo) and $warehouse_vars.retina_logo}data-retinalogo="{$warehouse_vars.retina_logo}" {/if} {if isset($logo_image_width) && $logo_image_width} width="{$logo_image_width}"{/if}{if isset($logo_image_height) && $logo_image_height} height="{$logo_image_height}"{/if} alt="{$shop_name|escape:'html':'UTF-8'}" />
											</a>
										</div>
                                        {if isset($HOOK_TOP)}{$HOOK_TOP}{/if}
                                        {hook h='iqitMegaMenu'}
										<div id="mh-search" class="header-mobile-search-button" data-mh-search="1"></div>
									</div>
                                {/if}
							</div>

							<div class="mobile-condensed-header mobile-style mobile-style{$warehouse_vars.mobile_header_style} {if $warehouse_vars.mobile_header_search}mobile-search-expanded{/if}">

                                {*{if $warehouse_vars.mobile_header_style == 1}*}
                                {*{include file="$tpl_dir./mobile-header1.tpl"}*}
                                {*{elseif $warehouse_vars.mobile_header_style == 2}*}
                                {*{include file="$tpl_dir./mobile-header2.tpl"}*}
                                {*{elseif $warehouse_vars.mobile_header_style == 3}*}
                                {include file="$tpl_dir./mobile-header3.tpl"}
                                {*{/if}*}

								<div class="mh-dropdowns">
									<div class="mh-drop mh-search-drop">
                                        {hook h='iqitMobileSearch'}
									</div>
								</div>

                                {*{hook h='iqitMobileHeader'}*}

							</div>

						</div>
					</div>
				</div>


				<div class="fw-pseudo-wrapper"> <div class="desktop-header">{hook h='maxHeader'} </div>	</div>
			{if isset($warehouse_vars.header_style) && $warehouse_vars.header_style == 1}
				<div class="sidebar-footer">
				{if isset($warehouse_vars.footer_img_src) and $warehouse_vars.footer_img_src}<div class="paymants_logos col-xs-12"><img class="img-responsive" src="{$link->getMediaLink($warehouse_vars.image_path)|escape:'html'}" alt="footerlogo" /></div>{/if}
					{if isset($warehouse_vars.copyright_text)}<div class="col-xs-12 cpr-txt"> {$warehouse_vars.copyright_text}  </div>{/if}
             		 
             	</div>
             {/if}

			</header>

			{if $page_name == 'index'}
			<div class="fw-pseudo-wrapper fw-pseudo-wrapper-slider">
			{hook h="displayTopColumn"}
			{hook h='maxSlideshow'} 
		</div>
			{/if}
		</div>
		{if $page_name !='index' && $page_name !='pagenotfound'}{if isset($warehouse_vars.breadcrumb_width) && $warehouse_vars.breadcrumb_width == 0}{include file="$tpl_dir./breadcrumb.tpl"}{/if}{/if}
		<div class="columns-container">
			<div id="columns" class="container">


				
				{if $page_name !='index' && $page_name !='pagenotfound'}
				{if isset($warehouse_vars.breadcrumb_width) && $warehouse_vars.breadcrumb_width == 1}{include file="$tpl_dir./breadcrumb.tpl"}{/if}
				{/if}
								<div class="fw-pseudo-wrapper">
				{if $page_name == 'index'}
				{hook h='maxInfos'} 
				{/if}
				{hook h='maxInfos2'} 
				
					</div>
				<div class="row content-inner">
				{if ($page_name == 'module-smartblog-category') or ($page_name == 'module-smartblog-details') or ($page_name == 'calculate')} {else}
					{if isset($warehouse_vars.left_on_phones) && $warehouse_vars.left_on_phones == 0}
					{if isset($left_column_size) && !empty($left_column_size)}
					<div id="left_column" class="column col-xs-12 col-sm-{$left_column_size|intval}">{$HOOK_LEFT_COLUMN}</div>
					{/if}
					{/if}
				{/if}
					{if isset($left_column_size) && isset($right_column_size)}{assign var='cols' value=(12 - $left_column_size - $right_column_size)}{else}{assign var='cols' value=12}{/if}
					<div id="center_column" class="center_column col-xs-12 {if ($page_name == 'module-smartblog-category') or ($page_name == 'module-smartblog-details') or ($page_name == 'calculate')} {else} col-sm-{$cols|intval} {if isset($warehouse_vars.left_on_phones) && $warehouse_vars.left_on_phones == 1} col-sm-push-{$left_column_size|intval}{/if}{/if}">
						{/if}
