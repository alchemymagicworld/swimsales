<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{onpay}warehouse>onpay_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{onpay}warehouse>onpay_f4d61ed23ce3c3fb8168e4a31c09b1bb'] = 'Логин:';
$_MODULE['<{onpay}warehouse>onpay_4c0295fa42920e4cf360479bf6285825'] = 'Пароль для API:';
$_MODULE['<{onpay}warehouse>onpay_6b5b3ce517b29886d000f89bc7d6fe09'] = 'Номер формы';
$_MODULE['<{onpay}warehouse>onpay_b17f3f4dcf653a5776792498a9b44d6a'] = 'Обновить настройки';
$_MODULE['<{onpay}warehouse>payment_cc94c4d0840af4b684daa79e0c38e240'] = 'Оплата картой с помощью OnPay';
