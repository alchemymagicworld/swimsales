<!-- Block header links module -->
<ul id="header_links" class="clearfix">
	{foreach from=$headerlinks_links item=headerlinks_link}
		{if isset($headerlinks_link.$lang)} 
			<li><a href="{$headerlinks_link.$url|escape}"{if $headerlinks_link.newWindow} target="_blank" {/if}>{$headerlinks_link.$lang|escape}</a></li>
		{/if}
	{/foreach}
	{if isset($showcontactlink) && $showcontactlink==1}<li id="header_link_contact"><a href="{$link->getPageLink('contact', true)|escape:'html'}" title="{l s='Contact' mod='headerlinks'}">{l s='Contact' mod='headerlinks'}</a></li>{/if}
	{if isset($showsitemaplink) && $showsitemaplink==1}<li id="header_link_sitemap"><a href="{$link->getPageLink('sitemap')|escape:'html'}" title="{l s='Sitemap' mod='headerlinks'}">{l s='Sitemap' mod='headerlinks'}</a></li>{/if}
    {if isset($title) && $title!=''} 
    {if (isset($showcontactlink) && $showcontactlink==1) || (isset($showsitemaplink) && $showsitemaplink==1)}<li class="separator">|</li>{/if}
    <li>{$title}</li>{/if}
</ul>
{include file="$tpl_dir./product-compare.tpl"}
<!-- /Block header links module -->
