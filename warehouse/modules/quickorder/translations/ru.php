<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{quickorder}warehouse>ajax_e61f52d02b9f52fbe1fd41ed320e29c3'] = 'Корзина не сформирована';
$_MODULE['<{quickorder}warehouse>ajax_26fde502196df93963ae0a6dabb79f85'] = 'Добавьте как минимум 1 товар';
$_MODULE['<{quickorder}warehouse>ajax_6e659c47c94d1e1dc7121859f43fb2b0'] = 'Эл.адрес введен неверно';
$_MODULE['<{quickorder}warehouse>ajax_39916f579f264041641c122e68e545d5'] = 'Введите номер телефона';
$_MODULE['<{quickorder}warehouse>ajax_4737d978c8ab5f98d5ad7fbd43b6b114'] = 'Имя не заполнено или содержит ошибку';
$_MODULE['<{quickorder}warehouse>ajax_6d2d62f9b2fa0f3d8748bc0ad36a1014'] = 'Фамилия не заполнена или содержит ошибку';
$_MODULE['<{quickorder}warehouse>ajax_5b2dfcaf7c9082b130744dc7d239cf9c'] = 'Адрес не заполнен или содержит ошибку';
$_MODULE['<{quickorder}warehouse>quickform_5c1e46c7bf74e65d9b5274dc767d8524'] = 'Быстрый заказ';
$_MODULE['<{quickorder}warehouse>quickform_4f6a5ab946214b0b5d5f9b888ca04528'] = 'Спасибо, заказ сформирован';
$_MODULE['<{quickorder}warehouse>quickform_3a752913039358fd0443516db6f49eec'] = 'Корзина пуста';
$_MODULE['<{quickorder}warehouse>quickform_668a8d8d7ffe5da112b266e46b79b685'] = 'Имя:';
$_MODULE['<{quickorder}warehouse>quickform_be5e0b5d50d48b049bd0f7b57cd163f9'] = 'Фамилия:';
$_MODULE['<{quickorder}warehouse>quickform_df1555fe48479f594280a2e03f9a8186'] = 'Эл.адрес:';
$_MODULE['<{quickorder}warehouse>quickform_cec59c4cf319e37788de60170226b705'] = 'Телефон:';
$_MODULE['<{quickorder}warehouse>quickform_2bf1d5fae1c321d594fdedf05058f709'] = 'Адрес:';
$_MODULE['<{quickorder}warehouse>quickform_240f3031f25601fa128bd4e15f0a37de'] = 'Комментарий:';
$_MODULE['<{quickorder}warehouse>quickform_fbcf2096e5b1f3c74db4c07315a69b8d'] = 'Нажмите для формирования заказа';
$_MODULE['<{quickorder}warehouse>quickform_6a3a15bb9cf9363939c4a185bab2baa8'] = 'Отправить заказ';
$_MODULE['<{quickorder}warehouse>quickorder_5c1e46c7bf74e65d9b5274dc767d8524'] = 'Быстрый заказ';
$_MODULE['<{quickorder}warehouse>quickorder_630b2a18632a272a07c6b637c4d9b3f6'] = 'Быстрый заказ для всей корзины (оформление заказа без регистрации)';
$_MODULE['<{quickorder}warehouse>quickorder_c888438d14855d7d96a2724ee9c306bd'] = 'Настройки обновлены';
$_MODULE['<{quickorder}warehouse>quickorder_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{quickorder}warehouse>quickorder_93cba07454f06a4a960172bbd6e2a435'] = 'Да';
$_MODULE['<{quickorder}warehouse>quickorder_b17f3f4dcf653a5776792498a9b44d6a'] = 'Обновить настройки';
