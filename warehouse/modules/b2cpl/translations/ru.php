<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{b2cpl}warehouse>b2cpl_3d7b4e871d6b919d5cb13160007f7cda'] = 'Доставка B2C';
$_MODULE['<{b2cpl}warehouse>b2cpl_df6a7e8a98e6f5e41ab093cf9ae00534'] = 'Кэш очищен';
$_MODULE['<{b2cpl}warehouse>b2cpl_55ec20ba0d6a1c0a7b035a0c84ebfd8a'] = 'Ошибка БД';
$_MODULE['<{b2cpl}warehouse>b2cpl_c9cc8cce247e49bae79f15173ce97354'] = 'Сохранить';
$_MODULE['<{b2cpl}warehouse>b2cpl_79c0d6cba080dc90b01c887064c9fc2f'] = 'Очистить кэш';
$_MODULE['<{b2cpl}warehouse>b2cpl_f4f70727dc34561dfde1a3c529b6205c'] = 'Настройки';
$_MODULE['<{b2cpl}warehouse>b2cpl_eae4c6c8dce6a00ff9af3ac60e98e0d6'] = 'id клиента';
$_MODULE['<{b2cpl}warehouse>b2cpl_897356954c2cd3d41b221e3f24f99bba'] = 'Ключ';
$_MODULE['<{b2cpl}warehouse>b2cpl_f1f142ad72e9ff4df217dae20a57abd3'] = 'Ключ для доступа к сервису';
$_MODULE['<{b2cpl}warehouse>b2cpl_4bbb8f967da6d1a610596d7257179c2b'] = 'Неверный';
$_MODULE['<{b2cpl}warehouse>b2cpl_f38f5974cdc23279ffe6d203641a8bdf'] = 'Настройки обновлены.';
$_MODULE['<{b2cpl}warehouse>form_a82be0f551b8708bc08eb33cd9ded0cf'] = 'Информация';
$_MODULE['<{b2cpl}warehouse>form_34b6cd75171affba6957e308dcbd92be'] = 'Версия';
$_MODULE['<{b2cpl}warehouse>form_794df3791a8c800841516007427a2aa3'] = 'Лицензия';
$_MODULE['<{b2cpl}warehouse>form_672caf27f5363dc833bda5099775e891'] = 'Разработчик';
$_MODULE['<{b2cpl}warehouse>form_b5a7adde1af5c87d7fd797b6245c2a39'] = 'Описание';
$_MODULE['<{b2cpl}warehouse>form_764a18c1d364a1eb563785d57137a945'] = 'Модули и шаблоны для PrestaShop';
