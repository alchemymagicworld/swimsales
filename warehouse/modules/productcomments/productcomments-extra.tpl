{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @version  Release: $Revision$
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if (!$content_only && (($nbComments == 0 && $too_early == false && ($is_logged || $allow_guests)) || ($nbComments != 0)))}
    <div id="product_comments_block_extra" class="no-print" itemprop="aggregateRating" itemscope
         itemtype="https://schema.org/AggregateRating">
        {if $nbComments != 0}
            <div class="comments_note clearfix">
                <span>{l s='Rating' mod='productcomments'}&nbsp;</span>
                <div class="star_content clearfix">
                    {section name="i" start=0 loop=5 step=1}
                        {if $averageTotal le $smarty.section.i.index}
                            <div class="star"></div>
                        {else}
                            <div class="star star_on"></div>
                        {/if}
                    {/section}
                    <meta itemprop="worstRating" content="0"/>
                    <meta itemprop="ratingValue"
                          content="{if isset($ratings.avg)}{$ratings.avg|round:1|escape:'html':'UTF-8'}{else}{$averageTotal|round:1|escape:'html':'UTF-8'}{/if}"/>
                    <meta itemprop="bestRating" content="5"/>
                </div>
            </div>
            <!-- .comments_note -->
        {/if}
        <a href="https://clck.yandex.ru/redir/dtype=stred/pid=47/cid=73582/path=dynamic.120x110/*https://market.yandex.ru/shop--swimsales-ru/371485/reviews">
            <img src="https://clck.yandex.ru/redir/dtype=stred/pid=47/cid=73581/path=dynamic.120x110/*https://grade.market.yandex.ru/?id=371485&action=image&size=1"
                 border="0"
                 alt="Читайте отзывы покупателей и оценивайте качество магазина SwimSales.ru на Яндекс.Маркете"/>
        </a>
        <ul class="comments_advices">
            {if $nbComments >= 0}
                <li>
                    <a onclick="reviewTabClick()" class="reviews">
                        {l s='Read reviews' mod='productcomments'} (<span itemprop="reviewCount">{$nbComments}</span>)
                    </a>
                </li>
            {/if}
            {if ($too_early == false AND ($is_logged OR $allow_guests))}
                <li>
                    <a class="open-comment-form" href="#new_comment_form">
                        {l s='Write a review' mod='productcomments'}
                    </a>
                </li>
            {/if}
        </ul>
    </div>
{/if}
{*<a href="https://clck.yandex.ru/redir/dtype=stred/pid=47/cid=73582/path=dynamic.120x110/*<a href="https://market.yandex.ru/shop--swimsales-ru/371485/reviews" target="_blank">https://market.yandex.ru/shop--swimsales-ru/371485/reviews</a>"&gt; &lt;img src="https://clck.yandex.ru/redir/dtype=stred/pid=47/cid=73581/path=dynamic.120x110/*<a href="https://grade.market.yandex.ru/?id=371485&amp;action=image&amp;size=1" target="_blank">https://grade.market.yandex.ru/?id=371485&amp;action=image&amp;size=1</a>" border="0" alt="Читайте отзывы покупателей и оценивайте качество магазина SwimSales.ru на Яндекс.Маркете" /&gt; &lt;/a&gt;</code>*}
<script>
    function reviewTabClick() {
        document.getElementById("tab_reviews_link").click();
    }
</script>
<!--  /Module ProductComments -->
