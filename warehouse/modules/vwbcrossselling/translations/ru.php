<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{vwbcrossselling}warehouse>crosssellingcontroller_490aa6e856ccf208a054389e47ce0d06'] = 'Id';
$_MODULE['<{vwbcrossselling}warehouse>crosssellingcontroller_af1b98adf7f686b84cd0b443e022b7a0'] = 'Категории';
$_MODULE['<{vwbcrossselling}warehouse>crosssellingcontroller_7deecf02e5daf6a7af8cb0030158b3b3'] = 'Категории подарка';
$_MODULE['<{vwbcrossselling}warehouse>crosssellingcontroller_068f80c7519d0528fb08e82137a72131'] = 'Товар';
$_MODULE['<{vwbcrossselling}warehouse>crosssellingcontroller_38070661d5ad384d9c7d21895dc4e784'] = 'Товар-подарок';
$_MODULE['<{vwbcrossselling}warehouse>crosssellingcontroller_b2f40690858b404ed10e62bdf422c704'] = 'Сумма корзины, на которую действует акция';
$_MODULE['<{vwbcrossselling}warehouse>crosssellingcontroller_104d9898c04874d0fbac36e125fa1369'] = 'Сумма подарков';
$_MODULE['<{vwbcrossselling}warehouse>crosssellingcontroller_4d3d769b812b6faa6b76e1a8abaece2d'] = 'Активность';
$_MODULE['<{vwbcrossselling}warehouse>crosssellingcontroller_ce5161e3ddba45b244b5a90189de9e51'] = '1+1';
$_MODULE['<{vwbcrossselling}warehouse>crosssellingcontroller_3d091e856cb7615d1ccb96bc759b5a92'] = 'Активность';
$_MODULE['<{vwbcrossselling}warehouse>crosssellingcontroller_179661f2585ddf22fb5e17d2586a1c3f'] = '1+1';
$_MODULE['<{vwbcrossselling}warehouse>crosssellingcontroller_c9cc8cce247e49bae79f15173ce97354'] = 'Сохранить';
$_MODULE['<{vwbcrossselling}warehouse>crosssellingcontroller_c8d273d56939753b79a6271552e77653'] = 'Выберите категории или продукт для акции';
$_MODULE['<{vwbcrossselling}warehouse>crosssellingcontroller_a162b71ce86a3b5e43a3781f33ded174'] = 'Выберите продукт или категорию подарка';
$_MODULE['<{vwbcrossselling}warehouse>crossselling_20cab89d3eb755499d1f526e94b451a1'] = 'Поздравляем! Вам доступен подарок!';
