<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{ecm_cmlid}warehouse>ecm_cmlid_e2f433fd9ae38757cf268f9171885588'] = 'CommerceML id';
$_MODULE['<{ecm_cmlid}warehouse>ecm_cmlid_f48e88beec7e307cfce343e299408d89'] = 'Добавляет вкладку для просмотра/изменения CommerceML идентификатора';
$_MODULE['<{ecm_cmlid}warehouse>ecm_cmlid_2e758b25088444fba4f36bb4b003b044'] = 'Возможно не установлен модуль синхронизации !!!';
$_MODULE['<{ecm_cmlid}warehouse>ecm_cmlid_1a502f679f6ab6eff31fee20955e1b1c'] = 'Просмотр, изменение CommerceML идентификатора для этого продукта';
$_MODULE['<{ecm_cmlid}warehouse>ecm_cmlid_deb10517653c255364175796ace3553f'] = 'Товара';
$_MODULE['<{ecm_cmlid}warehouse>ecm_cmlid_aaa084d45de97a43f63d8b7e320b184e'] = 'Название комбинаций';
$_MODULE['<{ecm_cmlid}warehouse>ecm_cmlid_47ac923d219501859fb68fed8c8db77b'] = 'Кобинаций';
$_MODULE['<{ecm_cmlid}warehouse>ecm_cmlid_e5aab8749b7b10512cd0d2707371cd46'] = 'Нет комбинаций';
$_MODULE['<{ecm_cmlid}warehouse>ecm_cmlid_a203c39a340e1d45069b1ba64e1d003a'] = 'Внимание!';
$_MODULE['<{ecm_cmlid}warehouse>ecm_cmlid_ff6fd709e450fa0b1c5024aeeb8991f6'] = 'Важное предупреждение';
$_MODULE['<{ecm_cmlid}warehouse>ecm_cmlid_baa8aaa5883aa92dbb2e23ff87f716db'] = 'При ошибочном изменении идентификатора, нарушится связь между товаром в магазине и в программе учета. Будте внимательны! ';
$_MODULE['<{ecm_cmlid}warehouse>ecm_cmlid_879f54910b2794afb674fc200c560b17'] = 'Считайте себя предупрежденными! Надеемся на ваше благоразумие!';
$_MODULE['<{ecm_cmlid}warehouse>infos_2e758b25088444fba4f36bb4b003b044'] = 'Возможно не установлен модуль синхронизации !!!';
$_MODULE['<{ecm_cmlid}warehouse>infos_60317b2d0b18c1bd6a17684ba0cf943f'] = 'Этот модуль позволяет просматривать/изменять значения CommerceML идентификаторов.';
$_MODULE['<{ecm_cmlid}warehouse>infos_b77b777fe2e1093f9592f49ee0cc0582'] = 'Эта возможность обеспечивается модулем сихронизации Prestashop с программой учета.';
$_MODULE['<{ecm_cmlid}warehouse>infos_c00c8ee9c3a4382d270dc939649f9b53'] = 'Контактная информация';
$_MODULE['<{ecm_cmlid}warehouse>infos_34b6cd75171affba6957e308dcbd92be'] = 'Версия';
$_MODULE['<{ecm_cmlid}warehouse>infos_672caf27f5363dc833bda5099775e891'] = 'Разработчик';
$_MODULE['<{ecm_cmlid}warehouse>infos_b5a7adde1af5c87d7fd797b6245c2a39'] = 'Описание';
$_MODULE['<{ecm_cmlid}warehouse>infos_93eebbf001447e3b80f611e4b8f1e71e'] = 'Электронный учет коммерческой деятельности	';
